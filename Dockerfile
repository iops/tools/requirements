# Dockerfile for testing requirements scripts

# ====================================================
ARG OS_IMAGE="alpine:3"
# ====================================================
FROM ${OS_IMAGE}

# build parameters
# -- os codename
ARG OS_NAME="ALP3X"
# -- workspace
ARG WORKSPACE="/workspace"
# -- parameters
ARG PARAMETERS="-v all"

# add requirements files
ADD * ${WORKSPACE}/

# switch to workspace dir
WORKDIR ${WORKSPACE}

# test commands
RUN echo "RUNNING TEST on [${OS_NAME}]" \
    # install bash first on ALP3X
    && if [ "ALP3X" = "${OS_NAME}" ];then apk update && apk add bash; fi \
    # run requirements script
    && bash requirements.sh ${PARAMETERS} \
    # workspace content
    && ls -la ${WORKSPACE}/* \
    # pip modules
    && pip list


# eof
