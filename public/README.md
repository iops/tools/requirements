# About this project
small modular framework for handling installation of **project requirements** in `CI/CD` projects


## Features
Currently, the following dependency types are supported.
- `requirements.ark`: list of apps to get using [arkade](https://github.com/alexellis/arkade)
- `requirements.ext`: list of selected tools to download
  - using provided **tools.download.TOOLNAME scripts** (must be provided)
  - by default, the **latest** available version will be downloaded
- `requirements.get`: list of external links to download (simply using **curl**)
- `requirements.git`: list of external git-repos to checkout (using [ansible-galaxy](https://galaxy.ansible.com/docs/using/installing.html))
- `requirements.img`: list of docker images to download (as tar.gz archive)
- `requirements.pip`: list of python modules to install (in current environment)
- `requirements.pkg`: list of os packages to install (in current environment)
- `requirements.txt`: same functionality as `requirements.pip`
- `requirements.xgz`: list of local/external archives (tgz|zip) to **fetch** & **extract**


## Getting Started

#### Add `requirements.sh` script to your project's repository
```
curl -sSLO https://ops4you.gitlab.io/tools/requirements/requirements.sh
```

#### Add other files corresponding to the type of dependencies required
```
curl -sSLO https://ops4you.gitlab.io/tools/requirements/requirements.pip
curl -sSLO https://ops4you.gitlab.io/tools/requirements/requirements.pkg
```

#### modify the requirements definitions to match your needs
```
#  edit list of required python modules
vi requirements.pip

# just ensure unzip is part of the os package dependencies
echo "unzip" >>requirements.pkg
```

#### Run the main `requirements.sh` script
This will fetch all defined dependencies to the current directory (by default):
```
bash requirements.sh
```

But you can also specify a destination path for all fetched artifacts
```
bash requirements.sh --dst-path /custom/path
```

Run `bash requirements.sh --help` to see all available options
```
=========
# USAGE #
=========
PURPOSE: fetch/install project requirements
SYNTAX:  requirements.sh [OPTIONS] <TYPE1> [[TYPE2] ... [TYPEn]]


===================
# SUPPORTED TYPES #
===================
all                       setup all available requirements
ext                       download defined external tools
get                       download defined external files (generic)
git                       download defined external git repos
img                       download defined docker images
pip                       install  defined pip packages
pkg                       install  defined os packages
xgz                       download and extract defined external archives (tgz|zip)


===========
# OPTIONS #
===========
-h|--help                 print out this help
-p|--dst-path PATH        set base path for extraction tasks (ext,get,git,img,tgz) DEFAULT=/projects/ops4you/tools/requirements.script
-t|--tmp-path PATH        set temp path for extraction tasks (ext,get,git,img,tgz) DEFAULT=/projects/ops4you/tools/requirements.script/tmp
-v|--verbose              enable verbose output (debugging infos)
```
